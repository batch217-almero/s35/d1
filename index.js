// DEPENDENCIES
const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 3001;

// ADD MONGODB CONNECTION
mongoose.connect("mongodb+srv://admin:admin123@zuitt.vd4owhx.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser:true, 
	useUnifiedTopology:true
});

// CHECK DB CONNECTION STATUS
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error")); //display error msg in console if meron

db.once("open", () => console.log("We're connected to the cloud database"));

// MONGOOSE SCHEMAS
// SCHEMA: Determines if structure of passed documents matches the document structure in database
// Acts as data blueprint and guide
const taskSchema = new mongoose.Schema({
	name : { 
		type: String,
		required: [true, "Task name is required"]
	},

	status : {
		type: String,
		default: "pending"
	}
});


// MONGOOSE MODELS
const Task = mongoose.model("Task", taskSchema);


// ADDITIONAL INIT SETTINGS
app.use(express.json());//converts all data from express to json format
app.use(express.urlencoded({extended:true}));//makes url reading compatible in different OS



// POST: CREATING A NEW TASK (A document)
app.post("/tasks", (request, response) => {
	Task.findOne({name: request.body.name}, (err, result) =>{
		if(result != null && result.name == request.body.name){
			return response.send("Duplicate found");
		}else {
			let newTask = new Task({
				name: request.body.name
			});

			newTask.save((saveErr, saveTask) => {
				if (saveErr) {
					return console.error(saveErr);
				}else{
					return response.status(201).send("New task created.");
				}
			})
		}
	})
});


// GET: Retrieve a Document
app.get("/tasks", (request, response) => {
	Task.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}else{
			return response.status(200).json({
				data : result
			})
		}
	})
});



// ACTIVITY ----------------------------------------------------
// STEP 1
const userSchema = new mongoose.Schema({
	username : { 
		type: String,
		required: [true, "Username is required"]
	},

	password : { 
		type: String,
		required: [true, "Password is required"]
	}
});

// STEP 2
const User = mongoose.model("User", userSchema);

// STEP 3
app.post("/signup", (request, response) => {
	User.findOne({username: request.body.username}, (err, result) =>{
		if(result != null && result.username == request.body.username){
			return response.send("Duplicate found");
		}else {
			let newUser = new User({
				username: request.body.username,
				password: request.body.password
			});

			newUser.save((saveErr, saveTask) => {
				if (saveErr) {
					return console.error(saveErr);
				}else{
					return response.status(201).send("New user registered");
				}
			})
		}
	})
})


// CALL LISTENER
app.listen(port, () => console.log(`Server is running at port ${port}`));